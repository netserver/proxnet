# -*- coding: utf-8 -*-
from re import compile
from os import popen


def search_ips_on(networks):
    ip_regex = compile(r'(?:[0-9]{1,3}\.){3}[0-9]{1,3}(?=\s|\)|$)')
    amounts_ips_on = 0
    ips_on = []
    return_ips_scan = popen(f'nmap -sP {networks}').read()
    found_on_ips = ip_regex.findall(return_ips_scan)
    ips_on.extend(found_on_ips)
    amounts_ips_on += len(ips_on)
    print(f'{amounts_ips_on} IPs ON.')
    print(ips_on)
