from conexao_banco import ConexaoBanco
import re


class InterageRedes():

    def __init__(self, rede='', ip='', prefixo=''):
        self.ConexaoBanco = ConexaoBanco()
        self.rede = rede
        self.ip = ip
        self.prefixo = prefixo

        self.calcula_ips()
        self.mascara_do_prefixo()

        print(self.ip, self.prefixo)

    def valida_rede(self):
        valida_rede = re.compile(
            '^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}/[0-9]{1,2}$')

        if not valida_rede.search(self.rede):
            valida_ip = re.compile(
                '^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$')

            if valida_ip.search(self.rede):
                raise ValueError("Insira prefixo.")
                return

            raise ValueError("Rede inválida.")
            return

        separa_ip_prefixo = self.rede.split('/')
        self.ip = separa_ip_prefixo[0]
        self.prefixo = separa_ip_prefixo[1]


if __name__ == '__main__':
    teste = InterageRedes(rede='192.168.25.0/24')
