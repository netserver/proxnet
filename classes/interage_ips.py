from conexao_banco import ConexaoBanco


class InterageIPs():
    def __init__(self):
        self.ConexaoBanco = ConexaoBanco()

    def insere_ips(self, rede, descricao):

        #    self.ConexaoBanco.runSQL(f"""INSERT INTO redes_ips (rede_ips, descricao)
        #                         VALUES ('{ips}, '{descricao}');""")

        for ip in rede:
            self.ConexaoBanco.runSQL(f"SELECT ip FROM ips_rede WHERE ip = '{ip}';")
            myresult = self.ConexaoBanco.cursor.fetchall()

            if not myresult:
                self.ConexaoBanco.runSQL(f"""INSERT INTO ips_rede (ip, status_ip, id_rede)
                                             VALUES ('{ip}', 1, 0);""")
                self.ConexaoBanco.mydb.commit()
                print(f'IP {ip} inserido.')
            else:
                print(f'IP {ip} já existe.')

    def atualiza_status_ip(self, ips):

        self.ConexaoBanco.runSQL(f"""UPDATE ips_rede set status_ip = 1
                                     WHERE ip IN ('{"', '".join(ips)}');""")
        self.ConexaoBanco.runSQL(f"""UPDATE ips_rede set status_ip = 0
                                     WHERE ip NOT IN ('{"', '".join(ips)}');""")
        self.ConexaoBanco.mydb.commit()
        print('Status atualizados.')

    def delete_ip(self, ips):

        for ip in ips:
            self.ConexaoBanco.runSQL(f"DELETE FROM ips_rede WHERE ip = '{ip}'")
            print(f'IP {ip} removido.')
        self.ConexaoBanco.mydb.commit()

    def select_ip(self, ips):
        # self.ConexaoBanco.runSQL("SELECT * FROM ips_rede;")
        self.ConexaoBanco.runSQL(f"""SELECT ip, status_ip FROM ips_rede
                                     WHERE ip IN ('{"', '".join(ips)}');""")
        myresult = self.ConexaoBanco.cursor.fetchall()
        print(myresult)


teste = InterageIPs()
ip = ['192.168.10.199', '192.168.0.9']
teste.select_ip(ip)
